use rand::seq::SliceRandom;
use rand::thread_rng;
use std::path::Path;

const NAMES: [&str; 4] = ["Dave", "Sarah", "Ollie", "Chaz"];
const DESCRIPTION: [&str; 4] = ["is a nob", "has nice hair", "is tall", "seems friendly"];

#[derive(serde::Deserialize, serde::Serialize)]
struct PageInfo {
    name: String,
    blocks: Vec<Block>,
}

#[derive(serde::Deserialize, serde::Serialize)] // Why do i need to do this?
struct Block {
    title: String,
    info: String,
}

#[async_std::main]
async fn main() -> tide::Result<()> {
    femme::start();
    log::debug!("Starting");
    let mut app = tide::new();
    app.with(tide::log::LogMiddleware::new());
    app.at("/test").get(test);
    app.at("/:pagename").get(wiki);
    app.listen("127.0.0.1:8080").await?;
    Ok(())
}

async fn test(_req: tide::Request<()>) -> tide::Result<String> {
    Ok("Hello World!".to_string())
}

// What if the array grows
/// pick_random_from_array is a function
fn pick_random_from_array(arr: [&str; 4]) -> String {
    let mut rng = thread_rng();
    arr.choose(&mut rng).unwrap().to_string()
}

fn read_in_yaml() -> Vec<Block> {
    if Path::new("database.yml").exists() {
        let f = std::fs::File::open("database.yml").expect("Could not open file");
        serde_yaml::from_reader(f).expect("Could not read values")
    } else {
        let new_data = build_test_data();
        let w = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open("database.yml")
            .expect("Couldn't open file");
        serde_yaml::to_writer(w, &new_data).unwrap();
        new_data
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_info_build() {
        let result = info_build("Robert");
        assert!(result.contains("Robert"))
    }
}

fn info_build(name: &str) -> String {
    format!("{} {}", name, pick_random_from_array(DESCRIPTION))
}

fn build_test_data() -> Vec<Block> {
    let mut test_vec = Vec::new();
    let mut count = 0;
    loop {
        let name = pick_random_from_array(NAMES);
        let person = Block {
            info: info_build(&name),
            title: name, // Im cloning here so I can use name again below, but this feels
                         // like the wrong move
        };

        count += 1;
        test_vec.push(person);

        if count == 400 {
            break;
        }
    }

    test_vec
}

async fn wiki(req: tide::Request<()>) -> tide::Result<tide::Body> {
    let pagename = req.param("pagename").unwrap_or("Missing");
    let pageblocks = read_in_yaml();
    let pageinfo = vec![PageInfo {
        name: pagename.to_string(),
        blocks: pageblocks,
    }];

    tide::Body::from_json(&pageinfo)
}
